from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.
from django.urls import reverse
class CustomUser(AbstractUser):
    age = models.PositiveIntegerField(null=True,blank=True)


    def get_absolute_url(self):
        return reverse('home')