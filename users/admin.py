from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
# Register your models here.
from .forms import CustomUserCreationForm,CustomUserCreationForm
from .models import CustomUser

class CustomAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserCreationForm
    model = CustomUser
    list_display = ['username','email','age','is_staff']

admin.site.register(CustomUser,CustomAdmin)