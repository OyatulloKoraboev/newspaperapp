from django.contrib import admin
from .models import Article, Comments


class CommentsInline(admin.TabularInline):
    model = Comments

class ArticleAdmin(admin.ModelAdmin):
    inlines = [
        CommentsInline
    ]



admin.site.register(Article)
admin.site.register(Comments)